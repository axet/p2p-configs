I see no future for REST internet. I see no future for Cloud Internet.

Internet is currently under heavy assault from government. And government control is growing over years.  People can't share content. Right now you can be targeted as individual for sharing a movie or music file.  People restricted to create content. You can't create screencasts and share it among your listeners without taking a risk been prosecuted by companies calming rights for your content.

It is clear we need to change it.

I'd like to see more people involved in developing P2P Internet. Where you have IP's bans, no DNS related issues.

We can turn the sides from been pirates to new government who rules the internet in online and offline in real live.

Just imagine nice opensource application, which can run on desktop machine or your mobile phone. After starting it you can find content by using QR code or secret ID to find a subscription. This content provided by person holding it's private key and uploading content to everyone who has a public key (ID or QR code). Content provided by this person encrypted. Only if you have a key you can decrypt it. Content can be automatically decrypted if subscriber feed author allows it to be public. To obtain decrypted key you can pay a bitcoin to subscriber feed author and he will send it using a p2p message. You can immediately decrypt the content and reshare it using new encryption key (yours) so you start to earn money from P2P users if they agrees to download it from you and pay you.

Everyone who downloading content from this subscription peer also sharing it so everyone boost the most popular content to speed it up (yea, like torrent does).

You can download a file just knowing it secret (global ID).

It also works offline! If you already have a content you can watch it, read it, or share in the private network not connected to global internet.

You can tag the content. Just by browsing internet you allowed to cache it's content and assign an unique ID so it would be easy to create your own Subscriber Feed and share content among your subscribers.

As you can see commercial also works here. You can share content with you ads.

It is compatible with old web based internet. You can continue browsing government / business controlled internet to follow the links or read another lie from your president.

What I see important to implements:

1) p2p configs

This is project will help you to store your application data in the private p2p cloud. If you lose your notebook, you can restore the data from your automatic backup archive created by p2p configs service running on your home router. Or you reading a book from your mobile phone using Read P2P configs application, which also running on your desktop machine. Current page should be synced back to your desktop, avoiding any cloud based (spying) software.

Every application instance has it's id, which holds the configuration. Configuration based on tree structure like git goes. And when you create conflict parameter change / current application keeps two values. It maybe usefull with different download folder parameter for each application instance on each device. So, parameters should have special types and different resolve logic (one shloud replace latest by it's date, another, keep two values separateley for two devices).

2) p2p share

Universal engine which can combine bittorrent functionality, edonkey protocol, dc++, btsync. Place where you can create share, download content, subscribe

  * https://github.com/axet/notes

3) p2p text

Send or read a email or take a video call. 

  * https://ring.cx/
